const merge = require('webpack-merge')
const common = require('./webpack.common.js')
const CleanWebpackPlugin = require('clean-webpack-plugin')

module.exports = merge(common, {
  stats: {
    reasons: true, // verbose errors
    chunks: false, // clean summary output
    colors: true
  },
  mode: 'development'
})
