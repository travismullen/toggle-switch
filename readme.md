# Custom Web Component using lit-html

[![js-standard-style](https://img.shields.io/badge/code%20style-standard-brightgreen.svg)](http://standardjs.com)

This can be included as a NPM dependnacy in any project.
Rendered component, as JavaScript module, can be found in `/dist`.

## Development (server)

```
  npm run dev
```

 `src/index.html` and `src/index.js` is only used for development and demo of component, and will not be included in final component. `init.mjs` is currently used for set-up and testing of component, but could be bypassed and set-up directly in the `src/index.js`.

## Build

```
  npm run build
```
  
Distributed `.js` file will take the name from `package.json`.
Compiled from `modules/main.component.mjs` as entry point.

[![js-standard-style](https://cdn.rawgit.com/standard/standard/master/badge.svg)](http://standardjs.com)
