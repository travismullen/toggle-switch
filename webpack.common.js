// webpack config
const path = require('path')
const config = require('./package.json')

const CleanWebpackPlugin = require('clean-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const ScriptExtHtmlWebpackPlugin = require('script-ext-html-webpack-plugin')

const libraryName = `${config.name}`
const renderedDirectory = 'dist'

// primary development set-up
module.exports = {
  mode: 'development',
  devServer: {
    // overlay: true, // overlay errors in the browser
    open: true, // will open the browser.
    contentBase: './dist'
  },
  resolve: {
    alias: {
      '~styles': path.resolve(__dirname, 'styles'),
      '~modules': path.resolve(__dirname, 'modules')
    },
    extensions: ['.mjs', '.js', '.json', '.scss']
  },
  entry: {
    webcomponentsjs: '@webcomponents/webcomponentsjs',
    polyfills: './polyfills.js',
    [libraryName]: './modules/main.component.js',
    app: './src/index.js'
  },
  output: {
    crossOriginLoading: 'anonymous',
    path: path.resolve(__dirname, renderedDirectory),
    // filename: '[name].[chunkhash].js',
    filename: '[name].js',
    libraryTarget: 'umd',
    library: libraryName,
    umdNamedDefine: true
  },
  plugins: [
    new CleanWebpackPlugin(renderedDirectory, {}),
    new HtmlWebpackPlugin({
      title: libraryName,
      template: './src/index.html'
    }),
    new ScriptExtHtmlWebpackPlugin({
      defaultAttribute: 'async',
      defer: [ 'webcomponentsjs', 'polyfills' ],
      module: [ libraryName ],
      prefetch: [ 'webcomponentsjs', 'polyfills' ]
    })
  ],
  module: {
    rules: [{
      enforce: 'pre',
      // set up standard-loader as a preloader
      test: /\.(js|mjs)$/,
      loader: 'standard-loader',
      exclude: /node_modules/,
      options: {
        fix: true
      }
    },
    {
      test: /\.(css|scss)$/,
      use: [{
        loader: 'css-loader',
        options: {
          importLoaders: 2 // 0 => no loaders (default); 1 => postcss-loader; 2 => postcss-loader, sass-loader
        }
      }, // translates CSS into CommonJS
      'postcss-loader',
      'sass-loader' // compiles Sass to CSS
      ]
    }]
  }
}
