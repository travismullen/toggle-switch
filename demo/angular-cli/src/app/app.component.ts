import { Component, AfterViewInit, ViewEncapsulation, ElementRef } from '@angular/core';

import '../../../../dist/toggle-switch';


// import standard mocks
import standardData from '../../../../modules/mock-data';
import mockAction from '../../../../modules/action';

@Component({
  encapsulation: ViewEncapsulation.None,
  selector: 'app-root',
  styleUrls: ['./app.component.scss'],
  templateUrl: './app.component.html'
})

// @add  implements OnInit
export class AppComponent implements AfterViewInit {
  constructor(private elementRef:ElementRef) {}

  collection: any[] = standardData;
  headlineKey: string = 'name';
  idKey: string = 'name';

  ngAfterViewInit () {
  	this.elementRef.nativeElement.querySelector('toggle-switch')
            .addEventListener('active-item', this.updateCollection.bind(this));
  }

	private updateCollection(event: CustomEvent) {
  	// const elm = this.elementRef.nativeElement.querySelector('toggle-switch').shadowRoot;
  	// console.log('toggle-item nodes', elm.querySelectorAll('toggle-item'));
  	// console.log('.toggle-thumb', elm.querySelector('.toggle-thumb'));
	  this.collection = mockAction(event, this.collection, this.idKey);
	  console.log(event.detail, this.collection);
	}
}