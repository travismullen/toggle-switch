
// import { name } from '../../../../package.json'
import mockData from '../../../../modules/mock-data';

const name = 'toggle-switch';

import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
      declarations: [
        AppComponent
      ],
    }).compileComponents();
  }));
  it('should create the wrapper component', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
  // it(`should have as title 'angular-cli'`, async(() => {
  //   const fixture = TestBed.createComponent(AppComponent);
  //   const app = fixture.debugElement.componentInstance;
  //   expect(app.title).toEqual('angular-cli');
  // }));
  it(`should render "${name}" as custom element`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    // console.log('name', name);
    expect(compiled.querySelector(name)).toBeTruthy();
  }));
  // it(`should set '.toggle-thumb' to active idKey value`, async(() => {
  //   const fixture = TestBed.createComponent(AppComponent);
  //   fixture.detectChanges();
  //   const compiled = fixture.debugElement.nativeElement;

  //   const titles = mockData.map(({ name }) => name );
  //   const elm = compiled.querySelector('toggle-switch').shadowRoot;
  //   const tags = elm.querySelectorAll('.toggle-thumb');
  //   console.log('titles', titles, tags);

  //   // for (let item of tags) {
  //   // console.log('ngAfterViewInit!!', item);
  //   //   expect(titles).toContain(item.querySelectorAll('h1').textContent);
  //   // }
  //   // console.log('ngAfterViewInit!!', elm, );
  //   // console.log('ngAfterViewInit!!', elm.querySelector('.toggle-thumb'));
  //   // expect(compiled.querySelector(name)).toBeTruthy();
  // }));
  // it('should render custom element in a h1 tag', async(() => {
  //   const fixture = TestBed.createComponent(AppComponent);
  //   fixture.detectChanges();
  //   const compiled = fixture.debugElement.nativeElement;
  //   console.log('compiled', compiled.shadowRoot);
  //   const tags = compiled.querySelectorAll('.toggle-thumb');

  //   const titles = mockData.map(({ name }) => name );

  //   console.log('tags', tags);
  //   // for (let element of tags) {
  //   //   element
  //   // }
  //   // expect(compiled.querySelectorAll('h1').textContent).toContain('Welcome to angular-cli!');
  // }));

  // should emmit event
  // should change data on even Listener
});
