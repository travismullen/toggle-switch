
/**
 * Removes all nodes from body that are not script tags.
 * Scripts tags are not removed because they have already impacted memory,
 * so we just want to delete any content they may have injected into the DOM.
 *
 * @return {Promise} - Resolves a NodeList of removed elements.
 */
export function cleanRoom () {
  return new Promise((resolve, reject) => {
    const nodes = document.body.querySelectorAll(':not(script)')
    if (nodes && nodes.length) {
      for (let node of nodes) {
        document.body.removeChild(node)
      }
    }
    resolve(nodes)
  })
}
