
const CUSTOM_EVENT_TYPE = 'pizza-delivery'

global.capturedEvents = []

async function eventCatcher (page) { 
  let handle = await page.evaluateHandle(() => {
    const elm = document.createElement('div')
    elm.id = `eventCatcher`
    elm.style = `top: -100vh`
    elm.style = `left: -100vw`
    elm.style = `width: 200vw`
    // elm.style = `z-index: 1`
    elm.style = `height: 200vh`
    elm.style = `background-color: hsla(95, 100%, 50%, 0.47)`
    document.body.appendChild(elm)
    return elm
  })
  return handle
}

// handles
let aParentElement
let aChildElement
let aDeepChildElement

let capturedEvents

async function pushEvent (newItem) {
  const eh = await page.evaluateHandle((state, data) => {
          // if (!window.capturedEvents) {
            state.push(data)
          // }
          console.log(' state',  state)
          return state.capturedEvents
      }, global.capturedEvents, newItem);
  return eh.dispose()
  // const resultHandle = await page.evaluateHandle((arr, item) => arr.push(item), aHandle, newItem);
  // console.log(await resultHandle.jsonValue());
  // await resultHandle.dispose();
}

// function eventCollector ({ type, detail, target, currentTarget, deepPath, timeStamp }) {
//   console.log(type, detail, deepPath, timeStamp)
//   window.capturedEvents.push({ type, detail, target, currentTarget, deepPath, timeStamp })
// }

before(async () => {
  eventCatcher(page)
})

describe(`Example: GoogleChrome/puppeteer`, () => {
  it('should define a window.onCustomEvent function on the page & capture a custom event on page load/navigation', async () => {
  // Define a window.onCustomEvent function on the page.
    await page.exposeFunction('onCustomEvent', (e) => {
      console.log("hello world", e)
      // this.myArray = ['my custom']
      // console.log('this.myArray', this.myArray)
      // console.log(window)
      // if (!window.capturedEvents) {
      //   window.capturedEvents = ['my first capture']
      // }
      // window.capturedEvents.push(e)
      // console.log(`${e.type} fired`, e.detail || '')
    })

    /**
   * Attach an event listener to page to capture a custom event on page load/navigation.
   * @param {string} type Event name.
   * @return {!Promise}
   */
    // function listenFor (type) {
    //   return page.evaluate(type => {
    //     // document.addEventListener(type, e => {
    //       // window.onCustomEvent({type, detail: e.detail})
    //     // })
    //       window.onCustomEvent(type)
    //       if (!window.capturedEvents) {
    //         window.capturedEvents = ['my first capture']
    //       }
    //   }, type)
    // }

    //   await page.waitFor('demo-stage')
    // // await page.goto('https://www.chromestatus.com/features', {waitUntil: 'networkidle0'})

    // await listenFor('someshit') // Listen for "app-ready" custom event on page load.

    // await listenFor('somenew-shit') // Listen for "app-ready" custom event on page load.
    await pushEvent('hello 2')
    await pushEvent('my new jawn')
    console.log(' global.capturedEvents',  global.capturedEvents)

  })
})

describe(`Custom Events`, () => {
  beforeEach(async () => {
    capturedEvents = await page.evaluateHandle(() => {
      if (!window.capturedEvents) {
        window.capturedEvents = ['is bound']
      }
      return window.capturedEvents
    })

    aParentElement = await page.evaluateHandle(() => {
      const elm = document.createElement('section')
      elm.classList = 'parent'
      elm.style = 'width: 200px'
      elm.style = 'height: 200px'
      elm.style = 'background-color: red'
      document.body.appendChild(elm)

      return elm
    })

    // await page.evaluateHandle((type, func, elm) => {
    //   elm.addEventListener(type, func)
    // }, CUSTOM_EVENT_TYPE, eventCollector, aParentElement)

    aChildElement = await page.evaluateHandle((parentElm) => {
      const elm = document.createElement('article')
      elm.classList = 'child'
      elm.style = 'width: 100px'
      elm.style = 'height: 100px'
      elm.style = 'background-color: blue'

      parentElm.appendChild(elm)

      return elm
    }, aParentElement)

    aDeepChildElement = await page.evaluateHandle((parentElm) => {
      const elm = document.createElement('p')
      elm.classList = 'deepchild'
      elm.style = 'width: 80px'
      elm.style = 'height: 80px'
      elm.style = 'background-color: purple'
      elm.innerText = 'Hello'

      parentElm.appendChild(elm)
      return elm
    }, aChildElement)
  })
  afterEach(async () => {
    console.log('global.capturedEvents', global.capturedEvents);
    // await page.evaluateHandle(elm => {
    //   document.body.removeChild(elm)
    // }, aParentElement)
    // await aParentElement.dispose()
  })
  describe(`should bubble`, () => {
    // it('should see events that are self dispathed', async () => {
    //   // add an appropriate event listener

    //   await page.waitFor('.parent')
    //   await page.evaluateHandle((elm, func) => {
    //     var obj = document.querySelector('.parent')
    //     // add an appropriate event listener
    //     obj.addEventListener('cat', function (e) { console.log(e.detail) })

    //     // create and dispatch the event
    //     var event = new CustomEvent('cat', {
    //       detail: {
    //         hazcheeseburger: true
    //       }
    //     })
    //     obj.dispatchEvent(event)
    //   }, aParentElement, eventCollector)

    //   const eventHistory = await page.evaluate(() => window.capturedEvents)
    //   console.log('eventHistory', eventHistory)
    //   // expect(element).to.exist
    // })
    // it('should render match id of test element', async () => {
    //   await page.waitFor(CUSTOM_ELEMENT)
    //   const idValue = await page.$eval(CUSTOM_ELEMENT, e => e.id)
    //   console.log('Test element id: ', idValue, testElementId)
    //   expect(idValue).to.equal(testElementId)
    // })
  })
})
