// use babel so we can polyfill what wepback was doing for us behind the scenes
// we want to be able to use import/export without module.export syntax
require('babel-register')
require('babel-polyfill')

const puppeteer = require('puppeteer')
const { expect, assert } = require('chai')

const { swap, restore, pending, inMemory } = require('./manageGlobals.js')

// puppeteer options
const opts = {
  headless: false,
  slowMo: 100,
  timeout: 10000,
  waitUntil: 'domcontentloaded'
}

// expose variables
before(async () => {
  // shared browser session
  swap('browser', await puppeteer.launch(opts))
  // shared page state
  swap('page', await browser.newPage())

  swap('expect', expect)
  swap('assert', assert)

  console.log(`global pointers to be swapped (includes previously undefined) - ${pending()}`)
  console.log(`any global variables that preexisted and will be held in alternative memory pointers - ${inMemory()}`)
})

// close browser and reset global variables
after(async () => {
  // browser.close()

  const restored = await restore()
  console.log(`global pointers that have be returned to state of proir to test...
    - ${restored}
  `)
  console.log(`global memory swaps still being held (should be none) - ${inMemory()} - ${pending()}`)
})

// confirm server is running!
require('./validate-server.js')

console.log('bootstrap loaded!')
