import data from './mock-data'
const action = ({detail}, source = data, idKey = 'id') => {
  const { id } = detail
  const collection = source
  const clear = collection.find(item => item.active === true)
  const activate = collection.find(item => item[idKey] === id)

  clear.active = false
  activate.active = true

  console.log('STATE CHANGED: ', id, collection)
  return collection
}
export default action
