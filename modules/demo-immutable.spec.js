// /* global CustomEvent: false, HTMLElement:false */

// const template = document.createElement('template')

// template.innerHTML = `
//   <style>
//     :host {
//       all: initial;
//       display: block;
//       contain: content;
//       /* text-align: center;
//       background: linear-gradient(to left, hotpink, transparent);
//       max-width: 500px;
//       margin: 0 auto;
//       border-radius: 8px;
//       transition: transform .2s ease-out; */
//     }

//     :host([hidden]) {
//       display: none;
//     }
//     article {
//       margin: 2rem;
//       padding: 2rem;
//       transition: background-color .5s ease-out;
//       background-color: hsla(95, 100%, 50%, 0.47);
//     }
//     article h1 {
//       text-align: center;
//       margin-top: 1rem;
//       color: white;
//     }
//   </style>
//   <article>
//     <h1></h1>
//     <ul></ul>
//   </article>
// `

// class ImmutableHistory extends HTMLElement {
//   constructor () {
//     super()

//     this.attachShadow({ mode: 'open' })
//     this.shadowRoot.appendChild(template.content.cloneNode(true))

//     this.displayActive = this.shadowRoot.querySelector('h1')
//     this.displayHistory = this.shadowRoot.querySelector('ul')
//     this.stream = []
//   }

//   _initView (ledger) {
//     for (const item of ledger) {
//       const elm = document.createElement('li')
//       elm.innerText = item
//       this.displayHistory.appendChild(elm)
//     }
//     console.log('this.displayHistory', this.displayHistory)
//   }
//   _updateView (item) {
//     const elm = document.createElement('li')
//     elm.innerText = item
//     this.displayHistory.appendChild(elm)
//   }

//   _updateHeadline (item) {
//     this.displayActive.innerText = item
//   }

//   _makeHistory (moment) {
//     this.dispatchEvent(new CustomEvent('history-made', {
//       [`${+(new Date())}`]: moment,
//       moment
//     }))
//     // this.dispatchEvent(new CustomEvent('trigger-item', {
//     //   detail: {
//     //     left: this.offsetLeft,
//     //     width: this.offsetWidth,
//     //     id: this.id,
//     //     headline: this.headline
//     //   },
//     //   bubbles: true,
//     //   composed: true
//     // }))
//   }

//   connectedCallback () {
//     if (!this.hasAttribute('headline')) {
//       this.setAttribute('headline', 'Stage for state preview.')
//     }
//     if (this.hasAttribute('bootstrap')) {
//       const bs = this.getAttribute('bootstrap')
//       if (!(bs && bs.length)) { return }
//       this.stream.unshift(...bs)
//     }
//     if (this.hasAttribute('immutable')) {
//       console.log('immutable', this.hasAttribute('immutable'))
//       const bs = this.getAttribute('immutable')
//       this.stream.unshift(bs)
//     }
//     this._initView(this.stream)
//   }

//   static get observedAttributes () {
//     return ['mutable', 'immutable']
//   }

//   attributeChangedCallback (name, oldValue, newValue) {
//     if (name === 'mutable') {
//       this._updateHeadline(this.headline)
//     }
//     if (name === 'immutable') {
//       this._makeHistory(this.immutable)
//       console.log(name, oldValue, newValue, this.headline)
//     }
//   }

//   get mutable () {
//     return this.getAttribute('mutable')
//   }

//   get immutable () {
//     return this.getAttribute('immutable')
//   }

//   set mutable (newValue) {
//     this.setAttribute('mutable', newValue)
//   }

//   set immutable (newValue) {
//     this.setAttribute('immutable', newValue)
//   }
// }

// window.customElements.define('immutable-history', ImmutableHistory)
