// /* eslint-disable no-unused-expressions */

// // structure in ./test/bootstrap.js
// // const devices = require('puppeteer/DeviceDescriptors');

// import mockData from './mock-data'
// // import service from '../test/ventriloquist'

// describe('Basic Custom Element', () => {
//   it('should render on page', async () => {
//     const element = await page.$('toggle-switch')
//     expect(element).to.exist
//   })
//   it('should have shadow DOM', async () => {
//     await page.waitFor('toggle-switch')

//     const elementHandle = await page.evaluateHandle(() => document.querySelector('toggle-switch'))
//     const resultHandle = await page.evaluateHandle(el => el.shadowRoot.innerHTML, elementHandle)
//     // console.log(await resultHandle.jsonValue());
//     expect(await resultHandle.jsonValue()).to.exist
//     await elementHandle.dispose()
//     await resultHandle.dispose()
//   })
//   it('should have deep shadow DOM', async () => {
//     await page.waitFor('toggle-switch')

//     // const element = await page.$('toggle-switch')

//     const elementHandle = await page.evaluateHandle(() => document.activeElement)
//     // const resultHandle = await page.evaluateHandle((el, sel) => el.shadowRoot.querySelector(sel).textContent, elementHandle, '.toggle-headline');
//     console.log('elementHandle', await elementHandle.jsonValue())
//     await elementHandle.dispose()
//     // await resultHandle.dispose();

//     // console.log('element', resultHandle);
//     // console.log('mockData', );
//     // await email.type( `invalid-email` );
//     // await submitBtn.click();
//     // await bs.page.waitFor( ASYNC_TRANSITION_TIMEOUT );
//     // const content = await element.$eval( '.toggle-thumb', el => el.innerHTML );
//     // await form.screenshot( png( `form-email-invalid-state` ) );
//     // console.log('content', content);
//     // expect( content ).to.exist;
//     // expect(await page.$$('toggle-switch')).to.have.lengthOf(1);
//   })
// })

// describe('Custom Element', () => {
//   it("should load 'name' value into '.toggle-headline' for item with 'active' status (true).", async () => {
//     await page.waitFor('toggle-switch')

//     const element = await page.$('toggle-switch')
//     // const element = await element.getProperty('idKey');

//     const elementHandle = await page.evaluateHandle(() => document.querySelector('toggle-switch'))
//     const resultHandle = await page.evaluateHandle((el, sel) => el.shadowRoot.querySelector(sel).textContent, elementHandle, '.toggle-headline')

//     // this is unique to data
//     const { name } = mockData.find(({ active }) => active === true)

//     console.log('\t .toggle-headline is: ', await resultHandle.jsonValue())
//     console.log('\t  active item value is: ', name)

//     expect(await resultHandle.jsonValue()).to.equal(name)

//     await elementHandle.dispose()
//     await resultHandle.dispose()
//   })

//   // it( "should get deep", async () => {
//   //   const elementHandle2 = await service.queryDeep(page, ['toggle-switch', 'toggle-item'])
//   //   console.log('elementHandle2', await elementHandle2.jsonValue());
//   //   // expect( await resultHandle.jsonValue() ).to.equal( name );
//   // });
// })
// // await page.$eval( '.nav_reveal', button => {
// //   button.click();
// // });
// // describe( "Email field", () => {
// //   it( "gets invalid state when invalid email typed in", async () => {
// //     const form = await bs.page.$( SEL_FORM ),
// //         submitBtn = await form.$( SEL_SUBMIT ),
// //         email = await form.$( SEL_EMAIL );

// //     await email.type( `invalid-email` );
// //     await submitBtn.click();
// //     await bs.page.waitFor( ASYNC_TRANSITION_TIMEOUT );
// //     const isInvalid = await form.$eval( `${SEL_EMAIL}`, el => el.matches( `:invalid` ) );
// //     await form.screenshot( png( `form-email-invalid-state` ) );
// //     expect( isInvalid ).toBeTruthy();

// //   });
// // });
