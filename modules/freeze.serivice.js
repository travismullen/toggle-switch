
// revive - https://en.wikipedia.org/wiki/ISO_8601
const dateFormat = /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}Z$/;
export function reviveDate(key, value) {
    if (typeof value === "string" && dateFormat.test(value)) {
        return new Date(value);
    }
    return value;
}


export function saveItem (key, value) {
  let toSave = value
  switch (Object.prototype.toString.call(value)) {
    case '[object Array]':
    case '[object Object]':
     toSave = JSON.stringify(value)
    default:
     localStorage[key] = toSave
  }

}

// function checkRevive (json) {
//  // const test = ['date', 'time', 'stamp']
//  for (let item of json) {
//    test.indexOf('date') || 
//    test.indexOf('date')
//  }
// }


