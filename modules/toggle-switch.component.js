/* global CustomEvent:false */

// import 'webcomponentsjs/webcomponents-loader.js';
// import 'web-animations-js';

import { name } from '../package.json'

import '~modules/toggle-item.component'
import { LitElement, html } from '@polymer/lit-element'
import { repeat } from 'lit-html/lib/repeat'

import ComponentStyles from '~styles/toggle-switch'

/**
 * # Awesome! An h1 heading
 * `<awesome-sauce>` injects a healthy dose of awesome into your page.
 * ## This is an h2 heading
 * In typical use, just slap some `<awesome-sauce>` at the top of your body:
 * <body>
 *   <awesome-sauce></awesome-sauce>
 * Wham! It's all awesome now!
 * @customElement
 * @polymer
 * @demo /demo/index.html
 *
 */

const toggleSwitchError = msg => new Error(`${name} - ${msg}`)

/**
 * @class My foobar class description.
 *
 */
class ToggleSwitch extends LitElement {
  static get properties () {
    return {
      /**
       * @property {Array} collection - Array of items to build the toggle list.
       */
      collection: Array,
      /**
       * @property {String} headlineKey - indicates the key name on data set to be used in display of headline.
       */
      headlineKey: String,
      /**
       * @property {String} activeKey - indicates the key name on data set to be used in to indicates the active state.
       */
      activeKey: String,
      /**
       * @property {String} idKey - indicates a unique indentifer which will be passed back by the 'active-item' event once the animation is complete.
       * value of propertyName passed back from the custom event once animation is complete
       */
      idKey: String,
      /**
       * @property {String} _headline - display within the '.toggle-thumb' element.
       */
      _headline: String
    }
  }

  constructor () {
    super()
    /**
     * @member {Promise} - Animation promise.
     */
    this._anim = null
    /**
     * @member - Create unique Id to target and avoid collision if multiple components exist in page.
     */
    this._thumbId = `thumb-${+(new Date())}`
    /**
     * @member - Left position.
     */
    this._left = 0
    /**
     * Width.
     */
    this._width = 0
    /**
     * Default headline to blank string.
     */
    this._headline = ''
    /**
     * Default active state property
     */
    this.activeKey = 'active'
  }

  _getActive () {
    // todo find active from activeKey value
    const item = this.collection.find(item => item[this.activeKey])
    // reference by unique id set in template render loop
    const id = item._id
    const element = this.shadowRoot.getElementById(id)

    this._setActive({
      left: element.offsetLeft,
      width: element.offsetWidth,
      headline: item[this.headlineKey],
      id
    })
  }

  // sanitizeId(name) {
  //   console.log('name', name);
  //     // remove all non alpha chars from begining for a valid DOM ID
  //     const begining = /^[^a-zA-Z]*/g
  //     let key = '';
  //     let unique = name.match(begining)
  //     key = name.replace(begining, '')
  //         .replace(/ /g, '-')
  //         .replace(/&/g, 'and');
  //     return `${key}-${unique}`;
  // }

  _didRender (props, changedProps, prevProps) {
    if (super._didRender) {
      super._didRender()
    }
    // only animate using _getActive
    // if the collection has changed
    if (changedProps.collection) {
      if (Object.prototype.toString.call(changedProps.collection) !== '[object Array]') {
        throw toggleSwitchError('Collection is not an Array!')
      } else if (changedProps.collection.length === 0) {
        throw toggleSwitchError('Collection has zero items. Add some items!')
      }
      this._getActive()
    }
  }

  _render ({ collection, _headline }) {
    // when used in some frameworks (like Angular)
    // wait for data to be bound before trying to render template
    if (!(collection && collection.length)) { console.warn('framework doing dumb shit.'); return }
    return html`
          <style>${ComponentStyles.toString()}</style>
          <div class="toggle-switch-wrapper">
            ${repeat(
    collection,
    (item, index) => {
      // use this loop to assign unique _id
      item._id = `toggle-item-${index}`
      return html`
                  <toggle-item
                    on-trigger-item="${({ detail }) => this._setActive(detail)}"
                    id="${item._id}"
                    headline="${item[this.headlineKey]}"
                    isActive="${item.active}" />`
    }
  )}
            <div id="${this._thumbId}" class="toggle-thumb">
              <h2 class="toggle-headline">${_headline}</h2>
            </div>
          </div>
    `
  }

  disconnectedCallback () {
    console.log('disconnectedCallback')
    if (this._anim && this._anim.cancel) {
      this._anim.cancel()
    }
  }

  // apply animation to all child nodes
  _applyFade (element, type, queueNext) {
    const childAnimations = []
    const animTiming = {
      duration: 200,
      fill: 'forwards',
      easing: 'ease-in'
    }
    // default is fadeout
    let keys = [
      { opacity: 0.0 },
      { opacity: 1.0 }
    ]
    if (type !== 'fadeIn') {
      keys = [
        { opacity: 1.0 },
        { opacity: 0.0 }
      ]
    }
    // get all animatable children
    for (let child of element.children) {
      if (child.animate && typeof (child.animate) === 'function') {
        let childAnimation = child.animate(keys, animTiming)
        childAnimation.pause()
        childAnimations.unshift(childAnimation)
        childAnimation.play()
      }
    }
    // put core animation on final promise
    // -- based on all are same time and order of array
    if (queueNext && childAnimations[0]) {
      childAnimations[0].onfinish = () => {
        this._anim.play()
      }
    }
  }

  _setActive ({ left, width, id, headline }) {
    const element = this.shadowRoot.getElementById(this._thumbId)

    // set-up core animation
    // -- from old position to new
    // -- update width
    this._anim = element.animate([
      {
        transform: `translateX(${this._left}px)`,
        width: `${this._width}px`
      },
      {
        transform: `translateX(${left}px)`,
        width: `${width}px`
      }
    ],
    {
      duration: 400,
      fill: 'forwards',
      easing: 'ease-out'
    })

    this._anim.pause()

    this._anim.onfinish = async () => {
      this._left = left
      this._width = width
      this._headline = headline
      this._applyFade(element, 'fadeIn', false)

      const item = this.collection.find(item => item._id === id)
      // signal that is item is actively selected now
      this.dispatchEvent(new CustomEvent('active-item', {
        detail: {
          selectorId: id,
          id: item[this.idKey],
          headline: headline
        },
        bubbles: true,
        composed: true
      }))
    }

    // wait for resizing/triggering to "stop"
    // clearTimeout(this.resize)
    // this.resize = setTimeout(() => {
    this._applyFade(element, 'fadeOut', true)
    // }, 250)
  }
}

window.customElements.define(`${name}`, ToggleSwitch)
