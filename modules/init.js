
import mockData from '~modules/mock-data'
import mockAction from '~modules/action'

// set-up component with javascript
const preloadLink = document.createElement('toggle-switch')
preloadLink.collection = mockData
preloadLink.idKey = 'name'
preloadLink.headlineKey = 'name'

preloadLink.addEventListener('active-item', event => {
  mockAction(event, mockData, preloadLink.idKey)

  const { color, name } = mockData.find(item => item.active === true)

  const stage = document.querySelector('demo-stage')
  stage.color = color
  stage.headline = name
  const history = document.querySelector('immutable-history')
  history.immutable = color
})

// document.body.appendChild(preloadLink)
