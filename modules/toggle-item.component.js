/* global CustomEvent:false */

// import 'node_modules/webcomponentsjs/webcomponents-loader';

import {LitElement, html} from '@polymer/lit-element'
import ComponentStyles from '~styles/toggle-item'
import '~modules/throttle-resize'

/**
 * # Awesome! An h1 heading
 * `<awesome-sauce>` injects a healthy dose of awesome into your page.
 * ## This is an h2 heading
 * In typical use, just slap some `<awesome-sauce>` at the top of your body:
 * <body>
 *   <awesome-sauce></awesome-sauce>
 * Wham! It's all awesome now!
 * @customElement
 * @demo /docs/index.html
 *
 */
class ToggleItem extends LitElement {
  static get properties () {
    return {
      /**
       * Headline displayed in the innerHTML of element.
       *
       * This could probably be replaced with a slot.
       * But keeping a text node for now for style consistancy.
       *
      */
      headline: String,
      /**
       * Is the element in an active state.
       * Only active should send 'trigger-item'
       * event on resize.
       *
      */
      isActive: Boolean
    }
  }

  ready () {
    if (super.ready) {
      super.ready()
    }
    window.addEventListener('optimizedResize', () => {
      if (this.isActive) {
        this._activateItem()
      }
    })
  }

  disconnectedCallback () {
    if (super.disconnectedCallback) {
      super.disconnectedCallback()
    }
    window.removeEventListener('optimizedResize', () => {
      if (this.isActive) {
        this._activateItem()
      }
    })
  }

  // @todo - put addEventListener on this?
  _render ({ headline }) {
    return html`
        <style>${ComponentStyles.toString()}</style>
          <div
            on-click="${() => this._activateItem()}">
              <h2 class="headline">${headline}</h2>
          </div>
        `
  }

  /**
   * Fired when `ToggleItem` changes its active state.
   *
   * Events must be annotated explicitly with an @event tag.
   *
   * Event properties are documented with the @param tag, just like
   * method parameters.
   *
   * @event trigger-item
   * @param {event} to apply preventDefault
   */
  _activateItem (event) {
    this.dispatchEvent(new CustomEvent('trigger-item', {
      detail: {
        left: this.offsetLeft,
        width: this.offsetWidth,
        id: this.id,
        headline: this.headline
      },
      bubbles: true,
      composed: true
    }))

    // if (event && event.preventDefault) {
    //   event.preventDefault()
    // }
  }
}

window.customElements.define('toggle-item', ToggleItem)
