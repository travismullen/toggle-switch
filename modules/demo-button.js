/* global HTMLElement:false, HTMLButtonElement:false */

/**
 * Quick Enumeration 
 * @constructor
 * @param {object} types - keys will serve as valid types. values will be results of test
 */
class Enumeration {
  constructor(obj) {
    for (const key in obj) {
      this[key] = obj[key]
    }
    return Object.freeze(this)
  }
  has (key) {
    return this.hasOwnProperty(key)
  }
}

const template = document.createElement('template')

// this does not work for `DemoButton`
// because HTMLButtonElement
// cannot have attachShadow applied
// but this shows how styles would be applied
// using `StyledDemoButton` or
// adding these style rules to global scope
template.innerHTML = `
  <style>
    :host {
      box-sizing: content-box;
      all: initial;
      contain: content;
    }

    [is="demo-button"] {
      height: 100px;
      width: 200px;
      color: black;
      background-color: yellow;
    }
    [is="demo-button"][active] {
      background-color: red;
    }
    /* bold all fonts in <slot/> */
    [is="demo-button"][active] slot {
      color: lime;
      font-weight: bold;
      letter-spacing: 1.3;
    }
    [is="demo-button"][complete] {
      background-color: purple;
    }
    [is="demo-button"][disabled] {
      color: white;
      background-color: black;
    }
  </style>
  <button is="demo-button"><slot></slot></button>
`
//** Enumeration of STATUS */
const STATUS = new Enumeration({
  disabled: 'disabled', // will set `disabled` attribute
  active: 'active',
  ready: 'ready',
})
//** DemoButton */
class DemoButton extends HTMLButtonElement {
  constructor () {
    super()

    //** cannot attachShadow to `HTMLButtonElement` */
    // this.attachShadow({ mode: 'open' })
    // this.shadowRoot.appendChild(template.content.cloneNode(true))
    /** 
    * this is for demo only
    * most cases addEventListener should be added to parent/controller
    */
    this.addEventListener('click', e => {
      console.log('DemoButton - event', e)
      this.status = STATUS.active
      setTimeout(() => {
        this.status = STATUS.ready
      }, 1000)
    });
  }

  static get observedAttributes () {
    return ['status']
  }

  attributeChangedCallback (name, oldValue, newValue) {
    this.updateStatus(newValue)
  }

  /**
   * Set the button's status.
   *
   * @param {STATUS} status - The button status. Must be an enumerated
   * value of {@link STATUS}.
   */
  updateStatus (newValue) {
    const key = newValue && newValue.toLowerCase()
    if (!STATUS.has(key)) { return }
    for (let state in STATUS) {
      this.removeAttribute(state)
    }
    this.setAttribute(key, 'true')
    // for demo only
    // this.innerHTML = newValue
  }

  get status () {
    return this.getAttribute('status')
  }

  set status (newValue) {
    this.setAttribute('status', newValue)
  }
}

window.customElements.define('demo-button', DemoButton, {extends: 'button'})

/** attributes to be passed down to `button` */
const supportedAttributes = [
  'autofocus',
  'form',
  'formnovalidate',
  'formaction',
  'name',
  'type',
  'value',
  'tabindex'
]
//** contained styles for `DemoButton` */
class StyledDemoButton extends HTMLElement {
  constructor () {
    super()

    this.attachShadow({ mode: 'open' })
    this.shadowRoot.appendChild(template.content.cloneNode(true))

    this.button = this.shadowRoot.querySelector('[is="demo-button"]')
  }

  static get observedAttributes () {
    return ['status', ...supportedAttributes]
  }

  attributeChangedCallback (name, oldValue, newValue) {
    //** pass other attribute changes, like type, down to button */
    this.button[name] = newValue
  }

  get status () {
    return this.getAttribute('status')
  }

  set status (newValue) {
    this.setAttribute('status', newValue)
  }
}

window.customElements.define('styled-demo-button', StyledDemoButton)


// ```html
// <button is="demo-button" status="disabled">Basic Button</button>
// <styled-demo-button status="disabled"><h1>Styled Button</h1></styled-demo-button>
// ```

// ```js
// // to test
// const elm = document.querySelector('[is="demo-button"]')
// // can change `status` with property change
// elm.status = 'ready'
// // or by setting attribute
// elm.setAttribute('status', 'disabled')
// elm.setAttribute('status', 'active')
// ```
