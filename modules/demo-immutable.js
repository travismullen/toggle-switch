/* global CustomEvent: false, HTMLElement:false */

const template = document.createElement('template')

template.innerHTML = `
  <style>
    :host {
      all: initial;
      display: block;
      contain: content;
      /* text-align: center;
      background: linear-gradient(to left, hotpink, transparent);
      max-width: 500px;
      margin: 0 auto;
      border-radius: 8px;
      transition: transform .2s ease-out; */
    }

    :host([hidden]) {
      display: none;
    }
    article {
      margin: 2rem;
      padding: 2rem;
      transition: background-color .5s ease-out;
      background-color: hsla(95, 100%, 50%, 0.47);
    }
    article h1 {
      text-align: center;
      margin-top: 1rem;
      color: white;
    }
  </style>
  <article>
    <h1></h1>
    <ul></ul>
  </article>
`

// @todo - extend from multiple classes

class Note extends Date {
  constructor () {
    super()
    // this.headline
    // this.detail
  }
  
  get headline () {
    return this.getAttribute('headline')
  }

  get detail () {
    return this.getAttribute('detail')
  }

  set headline (newValue) {
    this.setAttribute('headline', newValue)
  }

  set detail (newValue) {
    this.setAttribute('detail', newValue)
  }
}

class Payment extends Note {
  constructor () {
    super()
    this.amount
    this.currency
    this.buyer
    this.seller
  }
}


const historyModel = (body) => {
  return [`${+(new Date())}`]: body
}
class ImmutableHistory extends HTMLElement {
  constructor () {
    super()

    this.attachShadow({ mode: 'open' })
    this.shadowRoot.appendChild(template.content.cloneNode(true))

    this.displayActive = this.shadowRoot.querySelector('h1')
    this.displayHistory = this.shadowRoot.querySelector('ul')
    this.stream = []
  }

  _initView () {
    this.displayHistory.innerHTML = ''
    for (const item of this.stream.reverse()) {
      this._updateView(item)
    }
    console.log('this.displayHistory', this.stream)
  }

  // uniqe (arrArg) {
  //   return arrArg.filter((elem, pos, arr) => {
  //     return arr.indexOf(elem) == pos;
  //   });
  // }

  setData(inMemory){
    const local = JSON.parse(window.localStorage.getItem('history-made'));
    console.log('GET', local);
    const value = JSON.stringify([ ...new Set((local || []).concat(inMemory || [])) ])
    console.log('SET', value);
    window.localStorage.setItem('history-made', value);
    return value
  }

  clearData(){
    console.log('CLEAR');
    window.localStorage.clear()
  }
  

  _updateView (item) {
      console.log('_updateView', item)
    // using insertBefore
    // @see - https://developer.mozilla.org/en-US/docs/Web/API/Node/insertBefore

    // Create a new element
    const newElement = document.createElement("li");
    newElement.innerHTML = item

    // Get a reference to the first child
    const theFirstChild = this.displayHistory.firstChild;
    if (theFirstChild) {
      // Insert the new element before the first child
      this.displayHistory.insertBefore(newElement, theFirstChild);
    } else {
      console.log('no first!', this.displayHistory)
      this.displayHistory.appendChild(newElement);
    }
  }

  _updateHeadline (item) {
    this.displayActive.innerText = item
  }


  // @doto make model so data can be presented in the same way its

  _makeHistory (moment) {
    const value = historyModel(moment)
    this.dispatchEvent(new CustomEvent('history-made', {
      detail: value
    }))
    this.setData([value])
    // this.dispatchEvent(new CustomEvent('trigger-item', {
    //   detail: {
    //     left: this.offsetLeft,
    //     width: this.offsetWidth,
    //     id: this.id,
    //     headline: this.headline
    //   },
    //   bubbles: true,
    //   composed: true
    // }))
    this._updateView(value)
  }

  connectedCallback () {
    if (!this.hasAttribute('headline')) {
      this.setAttribute('headline', 'Stage for state preview.')
    }
    if (this.hasAttribute('immutable')) {
      console.log('immutable', this.hasAttribute('immutable'))
      const local = JSON.parse(window.localStorage.getItem('history-made'));
      const bs = this.getAttribute('immutable')
      this.stream.concat(local).unshift(bs)
    }
    window.addEventListener('storage', function(e) {  
     console.log('Woohoo, someone changed my localstorage va another tab/window!');
    });
  }

  static get observedAttributes () {
    return ['mutable', 'immutable']
  }

  attributeChangedCallback (name, oldValue, newValue) {
    if (name === 'mutable') {
      this._updateHeadline(this.mutable)
    }
    if (name === 'immutable') {
      this._makeHistory(this.immutable)
      // this._(this.immutable)
    }
     console.log(`changed ${this.mutable ? 'mutable' : 'imm' }`, name, oldValue, newValue, this.mutable || this.immutable)
  }

  get mutable () {
    return this.getAttribute('mutable')
  }

  get immutable () {
    return this.getAttribute('immutable')
  }

  set mutable (newValue) {
    this.setAttribute('mutable', newValue)
  }

  set immutable (newValue) {
    this.setAttribute('immutable', newValue)
  }

  set bootstrap (newValue) {
    if (!(newValue && newValue.length)) { return }
    this.stream.unshift(...newValue)
    this._initView()
  }
}

window.customElements.define('immutable-history', ImmutableHistory)

const init = () => {
  const elm = document.querySelector('immutable-history')
  elm.immutable = ['hello world' + new Date()]
}
