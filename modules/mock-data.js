const data = [
  { color: '#808000', name: 'Earth', order: 7, target: '#Earth' },
  { color: 'purple', name: 'Jupiter & Moons', order: 2, target: '#Jupiter', active: true },
  { color: 'red', name: 'Mars', order: 5, target: '#Mars' },
  { color: 'cyan', name: '123 Pluto', order: 9, target: '#Pluto' },
  { color: 'lime', name: '7Saturn', order: 3, target: '#Saturn' }
]
export default data
