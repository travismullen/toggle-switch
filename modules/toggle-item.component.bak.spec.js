// /* eslint-disable no-unused-expressions */

// import service from '../test/ventriloquist'
// import { cleanRoom } from '../test/tools'

// service.addMethod('headline')
// service.addMethod('isActive')

// const CUSTOM_ELEMENT = 'toggle-item'
// const CUSTOM_EVENT_TYPE = 'trigger-item'

// const testElementId = 'this-element-is-for-testing-only'
// let customElementCreate
// let emittedEvents // pointer to window.emittedEvents
// const emittedEventsListener = ({ detail, timeStamp }) => {
//   window.emittedEvents.push({ type, timeStamp, detail })
// }
// let docHandle // to removeEventListener

// describe(`Testing ${CUSTOM_ELEMENT}`, () => {
//   // before(async () => {
//   //   cleanRoom()
//   // })
//   // create "clean room"
//   // set-up and remove fresh instance for each test
//   beforeEach(async () => {
//     customElementCreate = await page.evaluateHandle((selector, id) => {
//       const elm = document.createElement(selector)
//       elm.headline = `hello world: ${+(new Date())}`
//       elm.id = id
//       document.body.appendChild(elm)
//       return elm
//     }, CUSTOM_ELEMENT, testElementId)
//   })
//   afterEach(async () => {
//     await page.evaluateHandle(elm => {
//       document.body.removeChild(elm)
//     }, customElementCreate)
//     await customElementCreate.dispose()
//   })

//   describe(` template display`, () => {
//     it('should render on page with content', async () => {
//       const element = await page.$eval(CUSTOM_ELEMENT, e => e.innerHTML)
//       expect(element).to.exist
//     })
//     it('should render match id of test element', async () => {
//       await page.waitFor(CUSTOM_ELEMENT)
//       const idValue = await page.$eval(CUSTOM_ELEMENT, e => e.id)
//       console.log('Test element id: ', idValue, testElementId)
//       expect(idValue).to.equal(testElementId)
//     })
//   })
//   describe(`template bindings`, () => {
//     it('should render property value .headline as textContent', async () => {
//       await page.waitFor(CUSTOM_ELEMENT)

//       // get current property value
//       const headlineValue = await page.$eval(CUSTOM_ELEMENT, e => e.headline)

//       // confirm it matches what is rendered
//       const elementHandle = await service.customElementHandle(CUSTOM_ELEMENT)
//       const resultHandle = await service.textContent(elementHandle, '.headline')

//       expect(await resultHandle.jsonValue()).to.equal(headlineValue)

//       // clean up
//       await elementHandle.dispose()
//       await resultHandle.dispose()
//     })
//     it('should render property value after it has been changed', async () => {
//       const testValue = 'value is updated!'
//       await page.waitFor(CUSTOM_ELEMENT)

//       // get current property value
//       const headlineValue = await page.$eval(CUSTOM_ELEMENT, e => e.headline)

//       // confirm it matches what is rendered
//       const elementHandle = await service.customElementHandle(CUSTOM_ELEMENT)
//       const resultHandle = await service.textContent(elementHandle, '.headline')

//       // sanity check (cannot assume clean room)
//       expect(await resultHandle.jsonValue()).to.equal(headlineValue)
//       await resultHandle.dispose()

//       // change property value
//       // const doUpdate = await page.evaluateHandle((el, update) => el.headline = update, elementHandle, testValue);
//       const doUpdate = await service.setHeadline(elementHandle, testValue)
//       // const updatedValue = await page.$eval(CUSTOM_ELEMENT, e => e.headline);
//       const updatedHandle = await service.textContent(elementHandle, '.headline')

//       // confirm it matches
//       expect(await updatedHandle.jsonValue()).to.equal(testValue)

//       await elementHandle.dispose()
//       await doUpdate.dispose()
//       await updatedHandle.dispose()
//     })
//   })

//       // testing custom events
//       // for resize
//       // 1. shouldent emmit an event without valid trigger
//       // 2. should emit even on trigger
//       // 3. should emit atleast X custom events for each trigger [event]
//       // repeat for click
//       // + 4. should prevent default of action (catch trigger /click?)
//       //

//   describe(`custom events`, () => {
//     beforeEach(async () => {
//       emittedEvents = await page.evaluateHandle(() => {
//         if (!window.emittedEvents) {
//           window.emittedEvents = []
//         }
//         return window.emittedEvents
//       })
//       await page.evaluateHandle((type, func) => {
//         window.addEventListener(type, func)
//         return window
//       }, CUSTOM_EVENT_TYPE, emittedEventsListener)
//     })
//     afterEach(async () => {
//       // await page.evaluateHandle((doc, type, func) => {
//       //   // remove listener
//       //   doc.removeEventListener(type, ({ detail, timeStamp }) => {
//       //     window.emittedEvents.push({ type, timeStamp, detail })
//       //   })
//       // }, docHandle, CUSTOM_EVENT_TYPE, emittedEventsListener)

//       // await page.evaluateHandle(emitted => {
//       //   // clean previous events
//       //   emitted = []
//       // }, emittedEvents)
//       // await docHandle.dispose()
//     })
//     it('should not emit event without valid triggers', async () => {

//       await page.waitFor(CUSTOM_ELEMENT)

//       // const elementHandle = await service.customElementHandle(CUSTOM_ELEMENT)
//       // const doUpdate = await service.setIsActive(elementHandle, 'true')

//       // console.log('elementHandle', await elementHandle.jsonValue())
//       // await page.setViewport({
//       //   height: 300,
//       //   width: 300
//       // })
//       // await page.setViewport({
//       //   height: 320,
//       //   width: 360
//       // })

//       const eventHistory = await page.evaluate(() => window.emittedEvents)
//       const validEvents = eventHistory.filter(({type}) => type === CUSTOM_EVENT_TYPE)

//       expect(validEvents).to.have.lengthOf(0)

//       // // clean up
//       // await elementHandle.dispose()
//       // await eventHistory.dispose()
//       // await doUpdate.dispose()
//       // await isActiveValue.dispose()

//     })
//     it('should not emit event valid resize event', async () => {

//       await page.waitFor(CUSTOM_ELEMENT)

//       // const elementHandle = await service.customElementHandle(CUSTOM_ELEMENT)
//       // const doUpdate = await service.setIsActive(elementHandle, 'true')

//       // await page.evaluateHandle(type => {
//       //   if (!window.emittedEvents) {
//       //     window.emittedEvents = []
//       //   }
//       //   document.addEventListener(type, ({ detail, timeStamp }) => {
//       //     window.emittedEvents.push({ type, timeStamp, detail })
//       //   })
//       // }, CUSTOM_EVENT_TYPE)

//       await page.setViewport({
//         height: 300,
//         width: 300
//       })
//       // await page.setViewport({
//       //   height: 320,
//       //   width: 360
//       // })

//       const eventHistory = await page.evaluate(() => window.emittedEvents)
//       console.log('eventHistory', eventHistory);
//       const validEvents = eventHistory.filter(({type}) => type === CUSTOM_EVENT_TYPE)

//       expect(validEvents).to.have.lengthOf(1)

//       // // clean up
//       // await elementHandle.dispose()
//       // await eventHistory.dispose()
//       // await doUpdate.dispose()
//       // await isActiveValue.dispose()

//       // await page.evaluateHandle(type => {
//       //   // clean previous events
//       //   window.emittedEvents = []
//       //   // remove listener
//       //   document.removeEventListener(type, ({ detail, timeStamp }) => {
//       //     window.emittedEvents.push({ type, timeStamp, detail })
//       //   })
//       // }, CUSTOM_EVENT_TYPE)
//     })
//     it('should not emit event without valid triggers', async () => {

//       await page.waitFor(CUSTOM_ELEMENT)

//       // const elementHandle = await service.customElementHandle(CUSTOM_ELEMENT)
//       // const doUpdate = await service.setIsActive(elementHandle, 'true')

//       // await page.evaluateHandle(type => {
//       //   if (!window.emittedEvents) {
//       //     window.emittedEvents = []
//       //   }
//       //   document.addEventListener(type, ({ detail, timeStamp }) => {
//       //     window.emittedEvents.push({ type, timeStamp, detail })
//       //   })
//       // }, CUSTOM_EVENT_TYPE)

//       await page.setViewport({
//         height: 300,
//         width: 300
//       })
//       await page.setViewport({
//         height: 320,
//         width: 360
//       })

//       const eventHistory = await page.evaluate(() => window.emittedEvents)
//       const validEvents = eventHistory.filter(({type}) => type === CUSTOM_EVENT_TYPE)

//       console.log('eventHistory', eventHistory);
//       expect(validEvents).to.have.lengthOf(2)

//       // // clean up
//       // await elementHandle.dispose()
//       // await eventHistory.dispose()
//       // await doUpdate.dispose()
//       // await isActiveValue.dispose()

//       // await page.evaluateHandle(type => {
//       //   // clean previous events
//       //   window.emittedEvents = []
//       //   // remove listener
//       //   document.removeEventListener(type, ({ detail, timeStamp }) => {
//       //     window.emittedEvents.push({ type, timeStamp, detail })
//       //   })
//       // }, CUSTOM_EVENT_TYPE)
//     })
//   })
// })
