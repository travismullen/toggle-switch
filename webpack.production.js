const merge = require('webpack-merge')
const common = require('./webpack.common.js')
const SriPlugin = require('webpack-subresource-integrity')

module.exports = merge(common, {
  plugins: [
    new SriPlugin({
      hashFuncNames: ['sha256', 'sha384'],
      enabled: true
    })
  ],
  mode: 'production'
})
