/* global HTMLElement:false */

import '~modules/init'

import '~modules/demo-button'
// import customElement() from '~modules/demo-stage'

// import '~modules/demo-stage'

// import '~modules/demo-immutable'

// Test to see if the browser supports the HTML template element by checking
// for the presence of the template element's content attribute.
if ('content' in document.createElement('template')) {
  console.log('Your browser is supports HTML template/content.')
  window.customElements.define('component-demo-stage',
    class extends HTMLElement {
      constructor () {
        super()
        let template = document.getElementById('component-demo-stage')
        let templateContent = template.content

        const shadowRoot = this.attachShadow({mode: 'open'})
          .appendChild(templateContent.cloneNode(true))
        console.log('shadowRoot', shadowRoot)
      }
    })
}
// let slottedSpan = document.querySelector('component-demo-stage span')
// console.log(slottedSpan.assignedSlot)
// console.log(slottedSpan.slot)
