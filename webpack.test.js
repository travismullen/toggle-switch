const merge = require('webpack-merge')
const common = require('./webpack.common.js')
// const HtmlWebpackPlugin = require('html-webpack-plugin')
// const SriPlugin = require('webpack-subresource-integrity')

module.exports = merge(common, {
  plugins: [
    // new HtmlWebpackPlugin({
    //   title: 'Testing...',
    //   template: './test/index.html'
    // }),
    // new SriPlugin({
    //   hashFuncNames: ['sha256', 'sha384'],
    //   enabled: true
    // })
  ],
  mode: 'production',
  devServer: {
    open: false,
    port: 8081
  }
})
